package com.smac89

import com.github.javaparser.JavaParser
import com.github.javaparser.ParserConfiguration
import groovy.transform.CompileStatic
import org.gradle.api.NonNullApi
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.file.FileTree
import org.gradle.api.logging.Logger
import org.gradle.api.tasks.util.PatternFilterable

/**
 * Creates an extension in the project called 'moduleName` which contains the
 * name of the current java module for this project.
 *
 * Assumes one java module per project
 */
@NonNullApi
@CompileStatic
class ModuleNamePlugin implements Plugin<Project> {
    private static final String EXT_NAME = 'moduleName'
    private Logger logger
    private Project project

    @Override
    void apply(Project target) {
        project = target
        logger = project.logger

        // Try to inject the moduleName as early as possible
        project.beforeEvaluate {
            configurePlugin()
        }
    }

    void configurePlugin() {
        if (project.extensions.findByName(EXT_NAME)) {
            return
        }

        FileTree src = findModInfoFile()

        src.eachWithIndex { File file, int index ->
            if (index == 0) {
                readModuleName(file).ifPresentOrElse {
                    logger.warn("Found module name: '$it'")
                    useModuleName(it.toString())
                } { useModuleName() }

            } else if (index == 1) { // Multiple `module-info.java`
                logger.warn("Found multiple `module-info.java` locations: $src.asPath. Only First one used")
                logger.warn("Each module should be in a separate project.")
            }
        }
    }

    /**
     * Finds the location of `module-info.java` in this project
     * @return The fileTree containing this file
     */
    FileTree findModInfoFile() {
        project.fileTree(project.projectDir).matching { PatternFilterable pattern ->
            pattern.include('**/module-info.java')
        }
    }

    /**
     * Given a name, sets that name as the moduleName for this project
     * @param defaultName The default name is set to the project's name
     */
    void useModuleName(String defaultName = project.name) {
        project.extensions.add(EXT_NAME, defaultName)
    }

    static Optional<String> readModuleName(File moduleInfo) {
        JavaParser parser = new JavaParser()
        parser.parserConfiguration.with {
            languageLevel = ParserConfiguration.LanguageLevel.JAVA_9
        }

        parser.parse(moduleInfo).result.flatMap { it.module }.map { it.name }.map {it.toString()}
    }
}
