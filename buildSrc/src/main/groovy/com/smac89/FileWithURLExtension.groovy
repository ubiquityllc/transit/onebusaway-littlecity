package com.smac89

import groovy.transform.CompileStatic

@CompileStatic
class FileWithURLExtension {
    static File temporaryFileFromUrl(final File type, URL url) {
        File file = File.createTempFile("file", "withurl")
        file.deleteOnExit()
        if (url) {
            file.withOutputStream { OutputStream out ->
                out.write(url.newInputStream().readAllBytes())
            }
        }
        return file
    }
}
