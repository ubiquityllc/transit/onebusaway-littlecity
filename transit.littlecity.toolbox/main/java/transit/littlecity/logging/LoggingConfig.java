package transit.littlecity.logging;

import java.util.Objects;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.InjectionPoint;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;
import org.springframework.core.MethodParameter;

@Configuration
public class LoggingConfig {
    /**
     * Method for injecting loggers into our application
     * @param injectionPoint The point where the logger injection is done
     * @return A logger configured for the class where this logger was defined
     */
    @Bean
    @Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
    public Logger logger(@NotNull InjectionPoint injectionPoint) {
        if (injectionPoint.getField() != null) {
            return LogManager.getLogger(injectionPoint.getField().getDeclaringClass().getCanonicalName());
        }
        MethodParameter parameter = Objects.requireNonNull(injectionPoint.getMethodParameter());
        return LogManager.getLogger(parameter.getDeclaringClass().getCanonicalName());
    }
}
