package transit.littlecity.fx.collections

import javafx.beans.Observable
import javafx.collections.FXCollections
import javafx.collections.ListChangeListener
import javafx.collections.ModifiableObservableListBase
import java.util.*
import java.util.concurrent.ConcurrentLinkedDeque
import kotlin.NoSuchElementException

/**
 * Base interface for observable queue
 */
interface ObservableDeque<T> : Deque<T>, Observable {
    /**
     * Alias for [[offer]]
     */
    fun enqueue(e: T) = offer(e)

    /**
     * Alias for [[remove]]
     */
    fun dequeue(): T = remove()

    fun addListener(listener: ListChangeListener<in T>)
    fun removeListener(listener: ListChangeListener<in T>)
}

interface LinkedChangeListener<T>: ListChangeListener<T> {
    override fun onChanged(c: ListChangeListener.Change<out T>?) {
        TODO(
            "not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}

/**
 * Extensions to the FXCollections class to allow creating an [[ObservableDeque]]
 */
fun <T> FXCollections.observableDeque(): ObservableDeque<T> = ObservableListDeque(ArrayDeque())

fun <T> FXCollections.synchronizedObservableDeque(): ObservableDeque<T> = ObservableListDeque(ConcurrentLinkedDeque())

internal abstract class ObservableListDequeBase<T>(private val deque: Deque<T>) :
        ModifiableObservableListBase<T>() {

    override fun get(index: Int): T? {
        return when (index) {
            0 -> deque.peekFirst()
            size - 1 -> deque.peekLast()
            else -> listIterator(index).next()
        }
    }

    override fun doRemove(index: Int): T? {
        return when (index) {
            0 -> deque.pollFirst()
            size - 1 -> deque.pollLast()
            else -> throw UnsupportedOperationException()
        }
    }

    override fun doSet(index: Int, element: T): T =
            throw UnsupportedOperationException()

    override fun doAdd(index: Int, element: T) {
        when (index) {
            0 -> deque.offerFirst(element)
            size - 1 -> deque.offerLast(element)
            else -> throw UnsupportedOperationException()
        }
    }
}

internal class ObservableListDeque<T>(private val deque: Deque<T>) :
        ObservableListDequeBase<T>(deque), ObservableDeque<T>, Deque<T> by deque {
    override val size: Int get() = deque.size

    override fun add(element: T): Boolean {
        addLast(element)
        return true
    }
    override fun addAll(elements: Collection<T>): Boolean = elements.all { add(it) }
    override fun clear() = super.clear()
    override fun contains(element: T): Boolean = deque.contains(element)
    override fun containsAll(elements: Collection<T>): Boolean = deque.containsAll(elements)
    override fun isEmpty(): Boolean = deque.isEmpty()
    override fun iterator(): MutableIterator<T> = deque.iterator()
    override fun remove(element: T): Boolean = removeFirstOccurrence(element)
    override fun removeAll(elements: Collection<T>): Boolean = super.removeAll(elements)
    override fun retainAll(elements: Collection<T>): Boolean = super.retainAll(elements)

    override fun addFirst(e: T) = add(0, e)
    override fun addLast(e: T) = add(size - 1, e)
    override fun remove(): T = removeFirst()
    override fun getFirst(): T = noElementIfNull(peekFirst())
    override fun getLast(): T = noElementIfNull(peekLast())

    override fun offer(e: T): Boolean = offerLast(e)

    override fun offerFirst(e: T): Boolean {
        add(0, e)
        return true
    }

    override fun offerLast(e: T): Boolean {
        add(size - 1, e)
        return true
    }

    override fun peek(): T? = peekFirst()
    override fun peekFirst(): T? = get(0)
    override fun peekLast(): T? = get(size - 1)

    override fun poll(): T? = pollFirst()
    override fun pollFirst(): T? = removeAt(0)
    override fun pollLast(): T? = removeAt(size - 1)

    override fun pop(): T = removeFirst()
    override fun push(e: T) = addFirst(e)

    override fun removeFirst(): T = noElementIfNull(pollFirst())
//    override fun removeFirstOccurrence(o: Any?): Boolean {
//        val i = indexOf(o)
//    }

    override fun removeLastOccurrence(o: Any?): Boolean {
        val i = deque.indexOf(o)
        when (i) {
            -1 -> return false
            0 -> pollFirst()
            size - 1 -> pollLast()
            else -> listIterator(size - 1).asSequence()
        }
        return true
    }

    override fun removeLast(): T = noElementIfNull(pollLast())

    companion object {
        fun <T> noElementIfNull(element: T?): T {
            if (null == element) {
                throw NoSuchElementException()
            }
            return element
        }
    }
}
