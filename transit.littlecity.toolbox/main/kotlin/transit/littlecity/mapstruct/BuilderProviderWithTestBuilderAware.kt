package transit.littlecity.mapstruct

import org.mapstruct.ap.spi.DefaultBuilderProvider
import javax.lang.model.element.ExecutableElement
import javax.lang.model.element.TypeElement

/**
 * A [org.mapstruct.ap.spi.BuilderProvider] implementation which ignores
 * test builder methods
 */
class BuilderProviderWithTestBuilderAware: DefaultBuilderProvider() {
    override fun isPossibleBuilderCreationMethod(method: ExecutableElement, typeElement: TypeElement): Boolean {
        return !method.simpleName.contains(TEST_BUILDER_PATTERN) &&
               super.isPossibleBuilderCreationMethod(method, typeElement)
    }

    companion object {
        private val TEST_BUILDER_PATTERN = Regex("forTests?$", RegexOption.IGNORE_CASE)
    }
}
