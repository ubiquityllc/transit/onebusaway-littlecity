package transit.littlecity.jacpfx

import org.jacpfx.api.message.Message
import org.jacpfx.rcp.util.FXUtil

/**
 * Checks if the expected message type corresponds to the type in the [Message]
 * and if so, calls the closure
 *
 * @param callback The code to execute if this type is what we expected
 */
inline fun <reified T, A, M> Message<A, M>.withTypedMessageBody(callback: (T) -> Unit): Boolean where T: M {
    if (isMessageBodyTypeOf(T::class.java)) {
        callback(this.getTypedMessageBody(T::class.java))
        return true
    }
    return false
}

/**
 * Checks that a message has the right value
 *
 * @param messageBodyValidator The value we are comparing it to
 * @param callback The closure which is called when this messageBody matches
 */
inline fun <reified T, A, M> Message<A, M>.withValidMessageBody(messageBodyValidator: (T) -> Boolean, callback: (T) -> Unit): Boolean where T : M {
    var success = false
    withTypedMessageBody { body: T ->
        if (messageBodyValidator(body)) {
            callback(body)
            success = true
        }
    }
    return success
}

fun <A, T> Message<A, T>.whenInitMessage(callback: () -> Unit): Boolean {
    if (messageBodyEquals(FXUtil.MessageUtil.INIT)) {
        callback()
        return true
    }
    return false
}
