package transit.littlecity.stopview

import javafx.animation.Animation
import javafx.animation.KeyFrame
import javafx.animation.Timeline
import javafx.beans.InvalidationListener
import javafx.beans.binding.Binding
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleObjectProperty
import javafx.beans.property.SimpleStringProperty
import javafx.event.Event
import javafx.event.EventHandler
import javafx.fxml.FXML
import javafx.scene.control.Label
import javafx.scene.layout.StackPane
import jfxtras.resources.JFXtrasFontRoboto
import org.apache.logging.log4j.Logger
import org.jacpfx.api.annotations.Resource
import org.jacpfx.api.annotations.lifecycle.OnHide
import org.jacpfx.api.annotations.lifecycle.OnShow
import org.jacpfx.api.annotations.lifecycle.PostConstruct
import org.jacpfx.api.annotations.perspective.Perspective
import org.jacpfx.api.message.Message
import org.jacpfx.rcp.componentLayout.PerspectiveLayout
import org.jacpfx.rcp.context.Context
import org.jacpfx.rcp.perspective.FXPerspective
import org.springframework.stereotype.Component
import transit.littlecity.api.domain.RouteUpdate
import transit.littlecity.api.domain.StopInformation
import transit.littlecity.api.domain.StopUpdate
import transit.littlecity.jacpfx.whenInitMessage
import transit.littlecity.jacpfx.withTypedMessageBody
import transit.littlecity.stopview.config.StopViewConfig
import transit.littlecity.stopview.data.SimpleRoute
import java.time.Duration
import java.time.LocalTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import java.util.*
import java.util.concurrent.Callable

@Perspective(
    name = "StopViewPerspective",
    id = StopViewConfig.STOP_VIEW_PERSPECTIVE_ID,
    viewLocation = StopViewConfig.STOP_VIEW_PERSPECTIVE_FXML,
    resourceBundleLocation = StopViewConfig.STOP_VIEW_RESOURCE,
    components = [StopViewConfig.STOP_GRID_VIEW_COMPONENT_ID]
)
@Component
class StopViewPerspective(private val logger: Logger) : FXPerspective {
    @FXML
    private lateinit var timeLabel: Label

    @Resource
    private lateinit var context: Context

    @FXML
    private lateinit var stopRoutes: StackPane

    @FXML
    private lateinit var vehicleLabel: Label

    @FXML
    private lateinit var nextArrival: Label

    @FXML
    private lateinit var lastSeen: Label

    private val clock = Timeline()

    private val updatesTimer = Timeline()

    private val time = SimpleStringProperty(this, "time")

    private val stopInformation = SimpleObjectProperty(this,
        "stopInformation", StopInformation.builderForTests().build())

    private val timezone: Binding<ZoneId>

    private val queuedRouteUpdates = Collections.synchronizedCollection(
        PriorityQueue<Pair<RouteUpdate, Int>>(compareBy({ it.second }, { it.first.nextArrival })))

    private val routeUpdatePriorities = mutableMapOf<String, Int>()

    init {
        timezone = Bindings.createObjectBinding(Callable{
            stopInformation.value.stopTimezone
        }, stopInformation)
    }

    override fun handlePerspective(message: Message<Event, Any>, perspectiveLayout: PerspectiveLayout?) {
        logger.info("Handle perspective")
        when {
            message.withTypedMessageBody(this::handleStopUpdate) -> {}
            message.withTypedMessageBody(this::handleStopInformation) -> {}
            message.whenInitMessage {
                logger.info("message init: " + message.messageBody)

                timeLabel.textProperty().bind(time)
            } -> {}
        }
    }

    /**
     * Handles the [StopInformation] message
     * @param message StopInformation
     */
    private fun handleStopInformation(message: StopInformation) {
        stopInformation.set(message)
    }

    /**
     * Handles the [StopUpdate] message
     * @param message StopUpdate
     */
    private fun handleStopUpdate(message: StopUpdate) {
        if (stopInformation.value?.stopId != message.stopId) {
            return
        }

        for (route in message.routeUpdates) {
            routeUpdatePriorities.computeIfPresent(route.routeId) { _, priority ->
                priority + 1
            }?.let { queuedRouteUpdates.add(route to it) }
        }
    }

    /**
     * Starts the timeline that updates the clock
     */
    private fun startClock() {
        clock.keyFrames.clear()
        clock.keyFrames += listOf(
            KeyFrame(javafx.util.Duration.ZERO, EventHandler {
                time.set(formatCurrentTime())
            }),
            KeyFrame(javafx.util.Duration.seconds(1.0))
        )
        clock.cycleCount = Timeline.INDEFINITE
    }

    private fun startRoutesSequence() {
        updatesTimer.keyFrames.clear()
        updatesTimer.keyFrames += listOf(
            KeyFrame(javafx.util.Duration.ZERO, EventHandler {
                queuedRouteUpdates.firstOrNull()?.first?.let { current ->
                    // Inform the grid view of this new change
                    context.send(StopViewConfig.STOP_GRID_VIEW_COMPONENT_ID, current.routeId)

                    vehicleLabel.text = current.vehicleName
                    nextArrival.text = "~${Duration.between(LocalTime.now(timezone.value),
                        LocalTime.ofInstant(current.nextArrival, timezone.value)).toMinutes()} mins"
                    lastSeen.text = "~${Duration.between(
                        LocalTime.ofInstant(current.previousArrival, timezone.value),
                        LocalTime.now(timezone.value)).toMinutes()} mins ago"
                }
            }),
            KeyFrame(javafx.util.Duration.seconds(30.0))
        )
        updatesTimer.cycleCount = Timeline.INDEFINITE
    }

    @Suppress("unused")
    @PostConstruct
    fun onStartPerspective(perspectiveLayout: PerspectiveLayout) {
        logger.warn("Perspective started!")
        startClock()
        startRoutesSequence()
        stopInformation.addListener(InvalidationListener {
            logger.info("Got new stop information!!")
            if (updatesTimer.status == Animation.Status.RUNNING) {
                updatesTimer.pause()
            }

            queuedRouteUpdates.clear()
            stopInformation.value.routeInformationList.map {
                it.routeId to 0
            }.toMap(routeUpdatePriorities)

            stopInformation.value.routeInformationList.forEach {
                context.send(StopViewConfig.STOP_GRID_VIEW_COMPONENT_ID,
                    SimpleRoute(it.routeColor.orElse("#ffa630"), it))
            }

            if (updatesTimer.status == Animation.Status.PAUSED) {
                updatesTimer.playFromStart()
            }
        })
        perspectiveLayout.registerTargetLayoutComponent(StopViewConfig.STOP_VIEW_GRID_SLOT,
            stopRoutes)
    }

    @Suppress("unused")
    @OnShow
    fun perspectiveShowing() {
        logger.warn("Perspective showing!")
        clock.play()
        updatesTimer.play()

        // TODO Remove after testing!
        context.send(StopInformation.builderForTests().build())
    }

    @Suppress("unused")
    @OnHide
    fun perspectiveHiding() {
        logger.warn("Perspective hiding!")
        clock.stop()
        updatesTimer.stop()
    }

    /**
     * Part of the binding that displays the current time.
     * This method formats the current time
     * @return The formatted time
     */
    private fun formatCurrentTime(): String {
        return LocalTime.now(timezone.value)
            .format(DateTimeFormatter.ofLocalizedTime(FormatStyle.SHORT))
    }

    companion object {
        init {
            JFXtrasFontRoboto.loadAll()
        }
    }
}
