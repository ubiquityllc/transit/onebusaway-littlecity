package transit.littlecity.stopview

import javafx.beans.property.SimpleObjectProperty
import javafx.collections.FXCollections
import javafx.event.Event
import javafx.fxml.FXML
import javafx.scene.Node
import org.jacpfx.api.annotations.Resource
import org.jacpfx.api.annotations.component.DeclarativeView
import org.jacpfx.api.annotations.lifecycle.PostConstruct
import org.jacpfx.api.message.Message
import org.jacpfx.rcp.component.FXComponent
import org.jacpfx.rcp.context.Context
import org.springframework.stereotype.Component
import org.tbee.javafx.scene.layout.MigPane
import transit.littlecity.jacpfx.withTypedMessageBody
import transit.littlecity.jacpfx.withValidMessageBody
import transit.littlecity.stopview.config.StopViewConfig
import transit.littlecity.stopview.data.SimpleRoute

@DeclarativeView(
    name = "StopGridViewComponent",
    id = StopViewConfig.STOP_GRID_VIEW_COMPONENT_ID,
    viewLocation = StopViewConfig.STOP_GRID_VIEW_FXML,
    initialTargetLayoutId = StopViewConfig.STOP_VIEW_GRID_SLOT
)
@Component
class StopGridViewComponent : FXComponent {
    @FXML
    lateinit var routesGridView: MigPane

    @Resource
    lateinit var context: Context

    private val routesForStop = FXCollections.observableMap(LinkedHashMap<String, Node>())

    private val selectedRouteNodeProperty = SimpleObjectProperty<Node>(this, "selectedRoute")

    @PostConstruct
    fun onStartComponent() {
        routesGridView.layoutConstraints
        selectedRouteNodeProperty.addListener { _, old, new ->
            old?.styleClass?.remove(CSS_SELECTED_CLASS)
            new.styleClass.add(CSS_SELECTED_CLASS)
        }
    }

    override fun handle(message: Message<Event, Any>): Node? {
        when {
            message.withValidMessageBody(routesForStop::containsKey) { routeId: String ->
                selectedRouteNodeProperty.set(routesForStop[routeId])
            } -> {}
        }
        return null
    }

    override fun postHandle(node: Node?, message: Message<Event, Any>): Node? {
        if (message.withTypedMessageBody(this::handleSimpleRoute)) return null
        return null
    }

    private fun handleSimpleRoute(route: SimpleRoute) {
        val fragment = context.getManagedFragmentHandler(StopRouteView::class.java)
        fragment.controller.simpleRoute.set(route)
        val node = fragment.fragmentNode
        routesForStop[route.routeId] = node // Used for lookup
        routesGridView.add(node)
    }

    companion object {
        const val CSS_SELECTED_CLASS = "route-selected"
    }
}
