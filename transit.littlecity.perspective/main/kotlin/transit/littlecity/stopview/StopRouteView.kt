package transit.littlecity.stopview

import javafx.beans.InvalidationListener
import javafx.beans.binding.Bindings
import javafx.beans.property.SimpleObjectProperty
import javafx.fxml.FXML
import javafx.geometry.Insets
import javafx.scene.control.Label
import javafx.scene.layout.Background
import javafx.scene.layout.BackgroundFill
import javafx.scene.layout.CornerRadii
import javafx.scene.layout.VBox
import org.jacpfx.api.annotations.fragment.Fragment
import org.jacpfx.api.annotations.lifecycle.PostConstruct
import org.jacpfx.api.fragment.Scope
import transit.littlecity.stopview.config.StopViewConfig
import transit.littlecity.stopview.data.SimpleRoute
import java.util.concurrent.Callable

@Fragment(
    id = StopViewConfig.STOP_ROUTE_VIEW_ID,
    viewLocation = StopViewConfig.STOP_ROUTE_VIEW_FXML,
    scope = Scope.PROTOTYPE
)
class StopRouteView {
    @FXML
    lateinit var routeLabel: Label

    @FXML
    lateinit var viewHolder: VBox

    val simpleRoute = SimpleObjectProperty<SimpleRoute>(this, "route")

    init {
        simpleRoute.addListener(InvalidationListener {
            routeLabel.text = simpleRoute.value.routeName
        })
    }

    @PostConstruct
    fun initialize() {
        viewHolder.backgroundProperty().bind(Bindings.createObjectBinding(Callable {
            val color = simpleRoute.value?.routeColor ?: return@Callable Background.EMPTY
            return@Callable Background(BackgroundFill(color, CornerRadii.EMPTY, Insets.EMPTY))
        }, simpleRoute))
    }
}
