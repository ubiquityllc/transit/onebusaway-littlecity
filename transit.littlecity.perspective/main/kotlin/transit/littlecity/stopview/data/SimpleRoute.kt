package transit.littlecity.stopview.data

import javafx.scene.paint.Color
import transit.littlecity.api.domain.RouteInformation

data class SimpleRoute(val routeColor: Color, val routeName: String, val routeId: String) {
    constructor(routeColor: String, routeUpdate: RouteInformation) :
        this(Color.web(routeColor), routeUpdate.routeShortName, routeUpdate.routeId)
}
