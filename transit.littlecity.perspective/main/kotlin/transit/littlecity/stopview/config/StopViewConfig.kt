package transit.littlecity.stopview.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("transit.littlecity.stopview")
open class StopViewConfig {
    companion object {
        const val STOP_VIEW_PERSPECTIVE_FXML = "/transit/littlecity/stopview/fxml/stopview.fxml"
        const val STOP_VIEW_PERSPECTIVE_ID = "stopViewPerspectiveId"
        const val STOP_VIEW_RESOURCE = "bundles.strings"
        const val STOP_VIEW_GRID_SLOT = "stopRoutesGrid"

        const val STOP_ROUTE_VIEW_ID = "stopRouteViewId"
        const val STOP_ROUTE_VIEW_FXML = "/transit/littlecity/stopview/fxml/stoprouteview.fxml"

        const val STOP_GRID_VIEW_COMPONENT_ID = "stopGridViewComponent"
        const val STOP_GRID_VIEW_FXML = "/transit/littlecity/stopview/fxml/stopgridview.fxml"
    }
}
