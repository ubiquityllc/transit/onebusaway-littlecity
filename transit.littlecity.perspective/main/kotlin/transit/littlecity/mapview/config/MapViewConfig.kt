package transit.littlecity.mapview.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * Configuration for the MapViewPerspective and it's components
 */
@Configuration
@ComponentScan("transit.littlecity.mapview")
open class MapViewConfig {
    companion object {
        const val MAP_COMPONENT_ID = "mapComponentId"
        const val MAP_COMPONENT_TARGET = "mapContent"
        const val MAP_VIEW_FXML = "/transit/littlecity/mapview/fxml/perspective.fxml"
        const val MAP_COMPONENT_FXML = "/transit/littlecity/mapview/fxml/mapcomponent.fxml"
        const val MAP_VIEW_PERSPECTIVE_ID = "mapViewPerspectiveId"
    }
}
