package transit.littlecity.mapview

import javafx.event.Event
import javafx.fxml.FXML
import javafx.scene.Node
import javafx.scene.control.TextField
import javafx.scene.layout.TilePane
import org.apache.logging.log4j.Logger
import org.jacpfx.api.annotations.component.DeclarativeView
import org.jacpfx.api.annotations.lifecycle.PostConstruct
import org.jacpfx.api.annotations.lifecycle.PreDestroy
import org.jacpfx.api.message.Message
import org.jacpfx.rcp.component.FXComponent
import org.springframework.stereotype.Component
import transit.littlecity.jacpfx.whenInitMessage
import transit.littlecity.mapview.config.MapViewConfig

@DeclarativeView(
    name = "MapComponent",
    id = MapViewConfig.MAP_COMPONENT_ID,
    active = true,
    viewLocation = MapViewConfig.MAP_COMPONENT_FXML,
    initialTargetLayoutId = MapViewConfig.MAP_COMPONENT_TARGET
)
@Component
class MapComponent(private val logger: Logger) : FXComponent {
    @FXML
    private lateinit var mapRoot: TilePane

    override fun handle(message: Message<Event, Any>): Node? {
        message.whenInitMessage {
        }
        return null
    }

    override fun postHandle(node: Node?, message: Message<Event, Any>): Node? {
        message.whenInitMessage {
            logger.info("MapView initialized!")
            val textField = TextField("Hello map")
            mapRoot.children.add(textField)
        }
        return null
    }

    @Suppress("unused")
    @PostConstruct
    fun initializeInteractive() {
        logger.info("Post construct")
    }

    @Suppress("unused")
    @PreDestroy
    fun preDestroy() {
        logger.info("predestroy: Closing the map!")
    }
}
