package transit.littlecity.mapview

import javafx.event.Event
import javafx.fxml.FXML
import javafx.scene.layout.StackPane
import org.apache.logging.log4j.Logger
import org.jacpfx.api.annotations.Resource
import org.jacpfx.api.annotations.lifecycle.OnHide
import org.jacpfx.api.annotations.lifecycle.OnShow
import org.jacpfx.api.annotations.lifecycle.PostConstruct
import org.jacpfx.api.annotations.perspective.Perspective
import org.jacpfx.api.message.Message
import org.jacpfx.rcp.componentLayout.FXComponentLayout
import org.jacpfx.rcp.componentLayout.FXMLPerspectiveLayout
import org.jacpfx.rcp.componentLayout.FXPerspectiveLayout
import org.jacpfx.rcp.componentLayout.PerspectiveLayout
import org.jacpfx.rcp.context.Context
import org.jacpfx.rcp.perspective.FXPerspective
import org.springframework.stereotype.Component
import transit.littlecity.jacpfx.whenInitMessage
import transit.littlecity.mapview.config.MapViewConfig
import java.util.*

@Suppress("UNUSED_PARAMETER")
@Perspective(
    name = "MapViewPerspective",
    id = MapViewConfig.MAP_VIEW_PERSPECTIVE_ID,
    viewLocation = MapViewConfig.MAP_VIEW_FXML,
    components = [MapViewConfig.MAP_COMPONENT_ID]
)
@Component
class MapViewPerspective(private val logger: Logger) : FXPerspective {
    @FXML
    private lateinit var mapContent: StackPane

    @Resource
    private lateinit var context: Context

    override fun handlePerspective(message: Message<Event, Any>, perspectiveLayout: PerspectiveLayout) {
        logger.info("handlePerspective: " + message.messageBody)

        message.whenInitMessage {
            logger.info("Handle perspective UI")
        }
    }

    @Suppress("unused")
    @PostConstruct
    fun onStartPerspective(perspectiveLayout: PerspectiveLayout,
                           layout: FXComponentLayout,
                           resourceBundle: ResourceBundle) {
        logger.warn("Perspective started!")
        perspectiveLayout.registerTargetLayoutComponent(
            MapViewConfig.MAP_COMPONENT_TARGET, mapContent)
    }

    @Suppress("unused")
    @OnShow
    fun perspectiveShowing(layout: FXPerspectiveLayout) {
        logger.info("Perspective showing")
//        context.send(MapViewConfig.MAP_COMPONENT_ID, new Expando(width: mapContent.width, height: mapContent.height))
    }

    @Suppress("unused")
    @OnHide
    fun perspectiveHiding(layout: FXMLPerspectiveLayout) {
        logger.info("Perspective hiding")
//        context.send()
    }
}
