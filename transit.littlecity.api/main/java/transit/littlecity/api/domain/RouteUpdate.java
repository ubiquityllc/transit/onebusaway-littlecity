package transit.littlecity.api.domain;

import java.time.Instant;
import java.util.Optional;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RouteUpdate {

    String routeId;
    String vehicleName;
    Instant previousArrival;
    Instant nextArrival;
    String routeAlert;

    public Optional<String> getRouteAlert() {
        return Optional.ofNullable(routeAlert);
    }

    public String getRouteName() {
        return vehicleName;
    }
}
