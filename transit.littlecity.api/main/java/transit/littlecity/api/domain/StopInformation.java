package transit.littlecity.api.domain;

import java.time.ZoneId;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import lombok.Builder;
import lombok.Value;

/**
 * Contains information about a particular stop
 */

@Value
@Builder
public class StopInformation {

    String stopId;
    String stopName;
    GeoLocation stopLocation;
    ZoneId stopTimezone;
    List<RouteInformation> routeInformationList;

    public static StopInformationBuilder builderForTests() {
        return builder().stopId(UUID.randomUUID().toString())
                        .stopName(UUID.randomUUID().toString())
                        .routeInformationList(
                            Stream.generate(() -> RouteInformation.builderForTests().build())
                                  .distinct()
                                  .limit(ThreadLocalRandom.current().nextInt(3, 5))
                                  .collect(Collectors.toUnmodifiableList()))
                        .stopLocation(GeoLocation.builderForTests().build())
                        .stopTimezone(ZoneId.systemDefault());
    }
}
