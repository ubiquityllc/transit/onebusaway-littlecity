package transit.littlecity.api.domain;

import java.util.Arrays;
import java.util.Optional;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class RouteInformation {

    String routeId;
    String routeLongName;
    String routeShortName;
    String routeColor;

    @SuppressWarnings("WeakerAccess")
    public static RouteInformationBuilder builderForTests() {
        return builder().routeId(UUID.randomUUID().toString())
                        .routeLongName(Arrays.asList("SILVERSPRING", "MEADOWGREEN", "LAWSON HEIGHTS", "BROADWAY")
                                             .get(ThreadLocalRandom.current().nextInt(4)))
                        .routeShortName(Arrays.asList("82", "27", "6", "60")
                                              .get(ThreadLocalRandom.current().nextInt(4)))
                        // Colors generated at: https://coolors.co/05668d-db3a34-97cc04-ffa630-00a6fb
                        .routeColor(Arrays.asList("#05668D", "#DB3A34", "#97CC04", "#FFA630", "#00A6FB")
                                          .get(ThreadLocalRandom.current().nextInt(5)));
    }

    public Optional<String> getRouteColor() {
        return Optional.ofNullable(routeColor);
    }
}
