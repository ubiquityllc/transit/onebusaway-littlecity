package transit.littlecity.api.domain.service;

import java.nio.ByteBuffer;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Stream;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.zeromq.ZMQ;
import org.zeromq.ZMQ.Socket;
import org.zeromq.core.ZContext;
import transit.littlecity.api.ApiService;
import transit.littlecity.api.domain.StopUpdate;
import transit.littlecity.api.handler.TransitStopHandler;
import transit.littlecity.api.mapper.StopInformationFBMapper;
import transit.littlecity.api.mapper.StopUpdateFBMapper;
import transit.littlecity.messaging.StopInformation;
import transit.littlecity.messaging.StopRoutesMessage;

/**
 * Listens for transit events and sends these to the handler Socket handling has to be done in a way
 * that works with zeromq See http://wiki.zeromq.org/whitepapers:0mq-termination
 */
@Service("busTransitApiService")
public class BusTransitService implements ApiService, DisposableBean {

    private final TransitStopHandler transitStopHandler;
    private final Logger logger;
    private final StopUpdateFBMapper stopUpdateFBMapper;
    private final StopInformationFBMapper stopInformationFBMapper;
    private final AtomicBoolean serviceRunning = new AtomicBoolean(false);
    private final ZContext context = new ZContext();
    private final ExecutorService socketsThreadPool = Executors.newFixedThreadPool(3);

    @Value("#{'tcp://' + '${server.updates.address}' + ':' + '${server.updates.port}'}")
    private String updatesSocketAddress;

    @Value("#{'tcp://' + '${server.states.address}' + ':' + '${server.states.port}'}")
    private String stateSocketAddress;

    @Value("${subscriptions}")
    private List<String> subscriptions;

    @Autowired
    public BusTransitService(TransitStopHandler transitHandler,
                             StopUpdateFBMapper stopUpdateFBMapper,
                             StopInformationFBMapper stopInformationFBMapper, Logger logger) {
        this.transitStopHandler      = transitHandler;
        this.stopUpdateFBMapper      = stopUpdateFBMapper;
        this.stopInformationFBMapper = stopInformationFBMapper;
        this.logger                  = logger;
        this.context.setContext(ZMQ.context(1));
    }

    private boolean serviceRunning() {
        return serviceRunning.get();
    }

    /**
     * Starts the processing of the messages. Expect {@link StopUpdate messages}.
     */
    @Override
    public void startService() {
        if (!serviceRunning.getAndSet(true)) {
            logger.info("Starting the zmq threads!!");
//            socketsThreadPool.submit(this::registerSubscriptions);
//            socketsThreadPool.submit(this::updatesSocketRun);
            logger.info("Started the zmq threads!!");
        }
    }

    private void registerSubscriptions() {
        try (Socket stateSocket = context.createSocket(ZMQ.PUSH)) {
            // Do this as early as possible to prevent spin lock
            // A socket that does `SEND` will block the entire context if
            // it does not have a peer to read the messages
            stateSocket.setLinger(0);
            stateSocket.setSndHWM(1);

            stateSocket.setPlainUsername("admin".getBytes(ZMQ.CHARSET));
            stateSocket.setPlainPassword("secret".getBytes(ZMQ.CHARSET));

            stateSocket.connect(stateSocketAddress);
            // Empty string is ignored but we have it here to make sure
            // the queue is filled, otherwise we might finish prematurely
            // and look like a fool when all the messages are dropped
            Stream.concat(Stream.of(""),
                          subscriptions.stream())
                  .takeWhile(s -> serviceRunning() && !Thread.currentThread().isInterrupted())
                  .peek(stopId -> logger.info("Registering stop id: {}", stopId))
                  .forEach(stateSocket::send);

            logger.info("Finished registering stop");
        }
    }

    /**
     * This method is ran in a separate thread
     *
     * It starts by establishing a connection to the server and then sends the subscriptions
     */
    private void updatesSocketRun() {
        try (Socket transitReceiver = context.createSocket(ZMQ.SUB)) {
            transitReceiver.setPlainUsername("admin".getBytes(ZMQ.CHARSET));
            transitReceiver.setPlainPassword("secret".getBytes(ZMQ.CHARSET));

            transitReceiver.connect(updatesSocketAddress);

            subscriptions.stream()
                         .map(String::getBytes)
                         .forEach(transitReceiver::subscribe);

            logger.printf(Level.INFO, "Connected to: %s", updatesSocketAddress);

            for (initTransitInfo(transitReceiver); serviceRunning() && !Thread.currentThread().isInterrupted(); ) {
                if (transitReceiver.recvStr(ZMQ.DONTWAIT, ZMQ.CHARSET) == null) {
                    continue;
                }

                logger.info("Receiving message...");
                byte[] msgBytes = null;

                if (transitReceiver.hasReceiveMore()) {
                    msgBytes = transitReceiver.recv();
                }

                if (Objects.requireNonNullElseGet(msgBytes, () -> new byte[0]).length == 0) {
                    // Possibly due to termination?
                    logger.info("Nothing to read!");
                    continue;
                }

                StopRoutesMessage stopRoutesMessage = StopRoutesMessage
                  .getRootAsStopRoutesMessage(ByteBuffer.wrap(Objects.requireNonNull(msgBytes)));

                transitStopHandler
                  .handleStopUpdate(stopUpdateFBMapper.stopMessageToStopUpdate(stopRoutesMessage));
            }
        }
    }

    private void initTransitInfo(Socket transitSocket) {
        while (serviceRunning() && !Thread.currentThread().isInterrupted()) {
            if (transitSocket.recvStr(ZMQ.DONTWAIT, ZMQ.CHARSET) == null) { // stop id
                continue;
            }

            logger.info("Received stop info");

            byte[] msgBytes = transitSocket.recv();

            if (msgBytes.length == 0) {
                // Possibly due to termination?
                logger.info("Nothing to read!");
                return;
            }

            StopInformation stopInformation = StopInformation
              .getRootAsStopInformation(ByteBuffer.wrap(msgBytes));

            transitStopHandler
              .initialize(stopInformationFBMapper.toStopInformation(stopInformation));

            break;
        }
    }

    @Override
    public void destroy() throws InterruptedException {
        serviceRunning.set(false);
        context.getContext().close();
        socketsThreadPool.shutdown();
        socketsThreadPool.awaitTermination(Integer.MAX_VALUE, TimeUnit.MILLISECONDS);
    }
}
