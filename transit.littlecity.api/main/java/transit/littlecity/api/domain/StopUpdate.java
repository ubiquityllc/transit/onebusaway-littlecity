package transit.littlecity.api.domain;

import java.util.List;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class StopUpdate {

    String stopId;
    List<RouteUpdate> routeUpdates;
}
