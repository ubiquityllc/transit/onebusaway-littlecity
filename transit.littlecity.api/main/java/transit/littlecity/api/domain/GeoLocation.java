package transit.littlecity.api.domain;

import java.util.concurrent.ThreadLocalRandom;
import lombok.Builder;
import lombok.Value;

@Value
@Builder
public class GeoLocation {

    double longitude;
    double latitude;

    @SuppressWarnings("WeakerAccess")
    public static GeoLocationBuilder builderForTests() {
        return builder().latitude(ThreadLocalRandom.current().nextDouble(0, 90))
                        .longitude(ThreadLocalRandom.current().nextDouble(0, 180));
    }
}
