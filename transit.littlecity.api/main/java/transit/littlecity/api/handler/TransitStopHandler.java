package transit.littlecity.api.handler;

import transit.littlecity.api.domain.StopInformation;
import transit.littlecity.api.domain.StopUpdate;

public interface TransitStopHandler {

    void initialize(StopInformation stopInformation);

    /**
     * Handle new updates about a stop
     *
     * @param busStop The information about the stop
     */
    void handleStopUpdate(StopUpdate busStop);
}
