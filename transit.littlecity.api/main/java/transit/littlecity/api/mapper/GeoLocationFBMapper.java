package transit.littlecity.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;
import transit.littlecity.api.domain.GeoLocation;
import transit.littlecity.messaging.StopInformation;

@Component
@Mapper
public interface GeoLocationFBMapper {

    @Mappings({
      @Mapping(target = "latitude", expression = "java(geoLocation.latitude())"),
      @Mapping(target = "longitude", expression = "java(geoLocation.longitude())"),
    })
    GeoLocation toGeoLoc(transit.littlecity.messaging.GeoLocation geoLocation);

    default GeoLocation stopInformationFBToGeoLocation(StopInformation stopInformation) {
        return toGeoLoc(stopInformation.stopLocation());
    }
}
