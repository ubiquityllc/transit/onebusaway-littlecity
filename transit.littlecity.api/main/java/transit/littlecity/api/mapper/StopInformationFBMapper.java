package transit.littlecity.api.mapper;

import java.time.ZoneId;
import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.mapstruct.Context;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.springframework.stereotype.Component;
import transit.littlecity.api.domain.RouteInformation;
import transit.littlecity.api.domain.StopInformation;

@Component
@Mapper(imports = {ZoneId.class, IntStream.class}, uses = {
  RouteInformationFBMapper.class,
  GeoLocationFBMapper.class
}, injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface StopInformationFBMapper {

    @Mappings({
      @Mapping(target = "stopId", expression = "java(stopInformation.stopId())"),
      @Mapping(target = "stopName", expression = "java(stopInformation.stopName())"),
      @Mapping(target = "stopTimezone", expression = "java(ZoneId.of(stopInformation.stopTimezone()))"),
      @Mapping(target = "stopLocation", source = "stopInformation"),
      @Mapping(target = "routeInformationList", expression = "java(routeInformationFromIndex(IntStream.range(0, stopInformation.routeInformationListLength()).boxed(), stopInformation))"),
    })
    StopInformation toStopInformation(transit.littlecity.messaging.StopInformation stopInformation);

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    List<RouteInformation> routeInformationFromIndex(Stream<Integer> positions,
                                                     @Context transit.littlecity.messaging.StopInformation stopInformation);

    default transit.littlecity.messaging.RouteInformation routeInformationAtIndex(Integer index,
                                                                                  @Context transit.littlecity.messaging.StopInformation stopInformation) {
        return stopInformation.routeInformationList(index);
    }
}
