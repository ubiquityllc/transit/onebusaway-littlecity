package transit.littlecity.api.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;
import transit.littlecity.api.domain.RouteInformation;

@Component
@Mapper
public interface RouteInformationFBMapper {

    @Mappings({
        @Mapping(target = "routeId", expression = "java(routeInformation.routeId())"),
        @Mapping(target = "routeLongName", expression = "java(routeInformation.routeLongName())"),
        @Mapping(target = "routeShortName", expression = "java(routeInformation.routeShortName())"),
        @Mapping(target = "routeColor", ignore = true)
    })
    RouteInformation toRouteInfo(transit.littlecity.messaging.RouteInformation routeInformation);
}
