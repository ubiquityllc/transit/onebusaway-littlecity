package transit.littlecity.api.mapper;

import java.time.Instant;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.springframework.stereotype.Component;
import transit.littlecity.api.domain.RouteUpdate;
import transit.littlecity.messaging.RouteArrivalDeparture;

@Component
@Mapper(imports = Instant.class)
public interface RouteUpdateFBMapper {

    /**
     * Convert {@link RouteArrivalDeparture} to {@link RouteUpdate}
     *
     * @param routeArrivalDeparture To be converted to a RouteUpdate object
     * @return The RouteUpdate object
     */
    @Mappings({
      @Mapping(target = "routeId", expression = "java(routeArrivalDeparture.routeId())"),
      @Mapping(target = "vehicleName", expression = "java(routeArrivalDeparture.routeVehicle())"),
      @Mapping(target = "previousArrival", expression = "java(Instant.ofEpochMilli(routeArrivalDeparture.lastSeen()))"),
      @Mapping(target = "nextArrival", expression = "java(Instant.ofEpochMilli(routeArrivalDeparture.nextArrival()))"),
      @Mapping(target = "routeAlert", ignore = true)
    })
    RouteUpdate routeArrivalToRouteUpdate(RouteArrivalDeparture routeArrivalDeparture);
}
