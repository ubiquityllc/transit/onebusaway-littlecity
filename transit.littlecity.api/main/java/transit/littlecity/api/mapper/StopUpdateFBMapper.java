package transit.littlecity.api.mapper;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;
import org.mapstruct.Context;
import org.mapstruct.IterableMapping;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.NullValueMappingStrategy;
import org.mapstruct.ReportingPolicy;
import org.springframework.stereotype.Component;
import transit.littlecity.api.domain.RouteUpdate;
import transit.littlecity.api.domain.StopUpdate;
import transit.littlecity.messaging.RouteArrivalDeparture;
import transit.littlecity.messaging.StopRoutesMessage;

@Component
@Mapper(imports = IntStream.class, unmappedTargetPolicy = ReportingPolicy.IGNORE, uses = RouteUpdateFBMapper.class)
public interface StopUpdateFBMapper {

    @Mappings({
      @Mapping(target = "stopId", expression = "java(stopRoutesMessage.stopId())"),
      @Mapping(target = "routeUpdates", expression = "java(routeUpdatesFromIndex(IntStream.range(0, stopRoutesMessage.routesArrivalsAndDeparturesLength()).boxed(), stopRoutesMessage))"),
    })
    StopUpdate stopMessageToStopUpdate(StopRoutesMessage stopRoutesMessage);

    @IterableMapping(nullValueMappingStrategy = NullValueMappingStrategy.RETURN_DEFAULT)
    List<RouteUpdate> routeUpdatesFromIndex(Stream<Integer> positions,
                                            @Context StopRoutesMessage stopRoutesMessage);

    default RouteArrivalDeparture routeArrivalDepartureAtIndex(Integer index,
                                                               @Context StopRoutesMessage stopRoutesMessage) {
        return stopRoutesMessage.routesArrivalsAndDepartures(index);
    }
}
