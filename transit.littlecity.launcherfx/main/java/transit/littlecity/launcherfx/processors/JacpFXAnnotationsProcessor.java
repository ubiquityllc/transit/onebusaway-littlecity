package transit.littlecity.launcherfx.processors;

import java.lang.annotation.Annotation;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.jacpfx.api.annotations.component.Component;
import org.jacpfx.api.annotations.component.DeclarativeView;
import org.jacpfx.api.annotations.component.Stateless;
import org.jacpfx.api.annotations.component.View;
import org.jacpfx.api.annotations.fragment.Fragment;
import org.jacpfx.api.annotations.perspective.Perspective;
import org.jacpfx.api.annotations.workbench.Workbench;
import org.jacpfx.rcp.component.CallbackComponent;
import org.jacpfx.rcp.registry.ClassRegistry;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.config.BeanFactoryPostProcessor;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.lang.NonNull;

@org.springframework.stereotype.Component
public class JacpFXAnnotationsProcessor implements BeanFactoryPostProcessor {

    private static final List<Class<? extends Annotation>> JACP_ANNOTATIONS = Arrays.asList(
      Component.class,
      Perspective.class,
      Fragment.class,
      View.class,
      Workbench.class,
      DeclarativeView.class,
      Stateless.class
    );
    private ConfigurableListableBeanFactory beanFactory;

    // Called before any bean is instantiated
    @Override
    public void postProcessBeanFactory(@NonNull ConfigurableListableBeanFactory beanFactory)
      throws BeansException {
        this.beanFactory = beanFactory;

        ClassRegistry.clearAllClasses();
        JACP_ANNOTATIONS.forEach(this::registerAnnotation);
        reScopeStateless();
    }

    private void registerAnnotation(Class<? extends Annotation> annotation) {
        List<Class> classes = new ArrayList<>();

        for (String beanName : beanFactory.getBeanNamesForAnnotation(annotation)) {
            BeanDefinition bean = beanFactory.getBeanDefinition(beanName);
            bean.setLazyInit(true);
            try {
                classes.add(Class.forName(bean.getBeanClassName()));
            } catch (ClassNotFoundException cnf) {
                cnf.printStackTrace();
            }
        }

        ClassRegistry.addClasses(classes);
    }

    /**
     * Stateless components should not be reused, therefore we make their scope as {@link
     * BeanDefinition#SCOPE_PROTOTYPE prototype}
     */
    private void reScopeStateless() {
        for (String beanName : beanFactory.getBeanNamesForType(CallbackComponent.class)) {
            final BeanDefinition beanDefinition = beanFactory.getBeanDefinition(beanName);
            try {
                Class<?> clazz = Class.forName(beanDefinition.getBeanClassName());
                if (clazz.isAnnotationPresent(Stateless.class)) {
                    beanDefinition.setScope(BeanDefinition.SCOPE_PROTOTYPE);
                }
            } catch (ClassNotFoundException cnf) {
                cnf.printStackTrace();
            }
        }
    }
}
