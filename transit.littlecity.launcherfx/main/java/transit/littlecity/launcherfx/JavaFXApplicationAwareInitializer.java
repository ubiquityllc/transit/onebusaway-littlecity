package transit.littlecity.launcherfx;

import static org.springframework.beans.factory.config.AutowireCapableBeanFactory.ORIGINAL_INSTANCE_SUFFIX;

import javafx.application.Application;
import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;

/**
 * This class can be used to register javafx applications as singletons so that the spring bean
 * processors do not try to create a new instance
 */
class JavaFXApplicationAwareInitializer implements
  ApplicationContextInitializer<ConfigurableApplicationContext> {

    private final Application javafxApplication;

    JavaFXApplicationAwareInitializer(Application javafxApplication) {
        this.javafxApplication = javafxApplication;
    }

    @Override
    public void initialize(ConfigurableApplicationContext applicationContext) {
        applicationContext.getBeanFactory().registerSingleton(getApplicationName(),
                                                              javafxApplication);
    }

    private String getApplicationName() {
        return javafxApplication.getClass().getCanonicalName() + ORIGINAL_INSTANCE_SUFFIX;
    }
}
