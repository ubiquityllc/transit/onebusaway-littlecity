package transit.littlecity.launcherfx;

import static org.springframework.beans.factory.config.AutowireCapableBeanFactory.AUTOWIRE_BY_NAME;

import java.util.Arrays;
import java.util.stream.Stream;
import javafx.stage.Stage;
import org.jacpfx.api.annotations.workbench.Workbench;
import org.jacpfx.api.exceptions.AnnotationNotFoundException;
import org.jacpfx.api.exceptions.AttributeNotFoundException;
import org.jacpfx.api.exceptions.ComponentNotFoundException;
import org.jacpfx.api.fragment.Scope;
import org.jacpfx.api.launcher.Launcher;
import org.jacpfx.minimal.launcher.AMinimalLauncher;
import org.jacpfx.rcp.workbench.AFXWorkbench;
import org.jacpfx.rcp.workbench.EmbeddedFXWorkbench;
import org.jacpfx.rcp.workbench.FXWorkbench;
import org.springframework.boot.Banner.Mode;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ConfigurableApplicationContext;

@SuppressWarnings("WeakerAccess")
public abstract class JacpFXSpringApplicationLauncher extends AMinimalLauncher {

    private final Class<? extends FXWorkbench> workbenchClass;
    private final Class<?>[] configSources;
    private ConfigurableApplicationContext springContext;

    protected JacpFXSpringApplicationLauncher(Class<? extends FXWorkbench> workbench,
                                              Class<?> appConfig,
                                              Class<?>... otherSources) {
        this.workbenchClass = workbench;
        this.configSources  = Stream.concat(Stream.of(appConfig),
                                            Arrays.stream(otherSources))
                                    .toArray(Class[]::new);
    }

    @Override
    public void init() {
        SpringApplication application = springAppBuilder().build();
        springContext = application.run();
    }

    @Override
    public void start(Stage stage) {
        initExceptionHandler();
        initWorkbench(stage);
        beforeSpringInit(stage);
        autowireApplication();
        afterSpringInit(stage);
    }

    protected void beforeSpringInit(Stage stage) {
        // does nothing by default
    }

    protected void afterSpringInit(Stage stage) {
        // does nothing by default
    }

    /**
     * Marshals a spring application builder which overriding classes can add on to
     *
     * @return The spring application builder
     */
    protected SpringApplicationBuilder springAppBuilder() {
        return new SpringApplicationBuilder(configSources)
          .logStartupInfo(false)
          // Can also use custom banner: https://www.baeldung.com/spring-boot-custom-banners
          .bannerMode(Mode.CONSOLE)
          .main(getClass())
          .headless(false)
          .initializers(new JavaFXApplicationAwareInitializer(this))
          .web(WebApplicationType.NONE);
    }

    /**
     * Initialize the workbench
     *
     * @param stage The application's stage
     */
    private void initWorkbench(final Stage stage) {
        final Class<? extends FXWorkbench> workbenchHandler = workbenchClass;
        final Launcher<?> launcher = getLauncher();

        if (workbenchHandler == null) {
            throw new ComponentNotFoundException("no FXWorkbench class defined");
        }
        if (!workbenchHandler.isAnnotationPresent(Workbench.class)) {
            throw new AnnotationNotFoundException("no @Workbench annotation found on class");
        }

        this.workbench = createWorkbench(launcher, workbenchHandler);
        workbench.init(launcher, stage);
    }

    /**
     * Uses spring to inject any dependencies required by this application
     */
    private void autowireApplication() {
        springContext.getAutowireCapableBeanFactory()
                     .autowireBeanProperties(this, AUTOWIRE_BY_NAME, true);
    }

    protected AFXWorkbench createWorkbench(final Launcher<?> launcher,
                                           final Class<? extends FXWorkbench> workbench) {
        final Workbench annotation = workbench.getAnnotation(Workbench.class);
        final String id = annotation.id();
        if (id.isEmpty()) {
            throw new AttributeNotFoundException("no workbench id found for: " + workbench);
        }
        final FXWorkbench handler = launcher
          .registerAndGetBean(workbench, id, Scope.SINGLETON);
        return new EmbeddedFXWorkbench(handler, getWorkbenchDecorator());
    }

    protected Launcher<? super ApplicationContext> getLauncher() {
        return new JacpFXSpringBeanLauncher(springContext);
    }

    protected void beforeAppShutdown() throws Exception {
        // Application-specific cleanup
    }

    @Override
    public final void stop() throws Exception {
        beforeAppShutdown();
        springContext.close();
    }

    @Override
    protected final String[] getBasePackages() {
        return new String[0];
    }

    @Override
    protected final void postInit(Stage stage) { }

    @Override
    public final Class<? extends FXWorkbench> getWorkbenchClass() {
        return workbenchClass;
    }
}
