package transit.littlecity.launcherfx;

import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.jacpfx.api.fragment.Scope;
import org.jacpfx.api.launcher.Launcher;
import org.springframework.beans.factory.support.AbstractBeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.context.ApplicationContext;

class JacpFXSpringBeanLauncher implements Launcher<ApplicationContext> {

    private final ApplicationContext context;
    private final Set<String> registeredNames;

    JacpFXSpringBeanLauncher(ApplicationContext context) {
        this.context = context;
        registeredNames = new HashSet<>();
    }

    @Override
    public ApplicationContext getContext() {
        return context;
    }

    @Override
    public <P> P getBean(Class<P> clazz) {
        return getContext().getBean(clazz);
    }

    @SuppressWarnings("unchecked")
    @Override
    public <P> P getBean(String qualifier) {
        return (P) getContext().getBean(qualifier);
    }

    @Override
    public <P> P registerAndGetBean(Class<? extends P> type, String id, Scope scope) {
        if (getContext().containsBean(id)) {
            return getContext().getBean(id, type);
        }

        BeanDefinitionBuilder b =
          BeanDefinitionBuilder.genericBeanDefinition(type)
                               .setScope(Optional.ofNullable(scope)
                                                 .orElse(Scope.SINGLETON)
                                                 .getType())
                               .setAutowireMode(AbstractBeanDefinition.AUTOWIRE_BY_TYPE);

        BeanDefinitionRegistry registry = (BeanDefinitionRegistry) getContext()
          .getAutowireCapableBeanFactory();

        // We want to ensure that only the names we explicitly registered
        // are registered in the bean registry
        for (String name : getContext().getBeanNamesForType(type)) {
            // if we did not register this bean for the given type...
            if (!registeredNames.contains(name)) {
                // remove it from the bean registry
                registry.removeBeanDefinition(name);
            }
        }
        // register the bean in the bean registry
        registry.registerBeanDefinition(id, b.getBeanDefinition());
        registeredNames.add(id);

        return getBean(id);
    }
}
