package transit.littlecity.launcherfx;

import java.util.Objects;
import java.util.Optional;
import java.util.function.Function;
import java.util.stream.Stream;
import org.jacpfx.api.annotations.component.Component;
import org.jacpfx.api.annotations.component.DeclarativeView;
import org.jacpfx.api.annotations.component.View;
import org.jacpfx.api.annotations.perspective.Perspective;
import org.jacpfx.api.annotations.workbench.Workbench;
import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanNameGenerator;
import org.springframework.context.annotation.AnnotationBeanNameGenerator;
import org.springframework.lang.NonNull;

public class JacpFXBeanNameGenerator implements BeanNameGenerator {

    private AnnotationBeanNameGenerator delegate = new AnnotationBeanNameGenerator();

    @NonNull
    @Override
    public String generateBeanName(@NonNull BeanDefinition definition,
                                   @NonNull BeanDefinitionRegistry registry) {
        Optional<String> maybeName = Optional.empty();
        try {
            Class clazzForBean = Class.forName(Objects.requireNonNull(definition.getBeanClassName()));
            maybeName = Stream.<Function<Class<?>, Optional<String>>>of(
              this::workbenchId,
              this::perspectiveId,
              this::viewId,
              this::componentId,
              this::declarativeId)
              .map(f -> f.apply(clazzForBean))
              .filter(Optional::isPresent)
              .map(Optional::get)
              .findFirst();
        } catch (ClassNotFoundException cnf) {
            cnf.printStackTrace();
        }
        return maybeName.orElseGet(() -> delegate.generateBeanName(definition, registry));
    }

    private Optional<String> perspectiveId(Class<?> clazz) {
        return Optional.ofNullable(clazz.getAnnotation(Perspective.class)).map(Perspective::id);
    }

    private Optional<String> viewId(Class<?> clazz) {
        return Optional.ofNullable(clazz.getAnnotation(View.class)).map(View::id);
    }

    private Optional<String> componentId(Class<?> clazz) {
        return Optional.ofNullable(clazz.getAnnotation(Component.class)).map(Component::id);
    }

    private Optional<String> declarativeId(Class<?> clazz) {
        return Optional.ofNullable(clazz.getAnnotation(DeclarativeView.class))
                       .map(DeclarativeView::id);
    }

    private Optional<String> workbenchId(Class<?> clazz) {
        return Optional.ofNullable(clazz.getAnnotation(Workbench.class)).map(Workbench::id);
    }
}
