package transit.littlecity;

import javafx.event.Event;
import javafx.scene.Node;
import javafx.stage.Stage;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.Logger;
import org.jacpfx.api.annotations.Resource;
import org.jacpfx.api.annotations.workbench.Workbench;
import org.jacpfx.api.componentLayout.WorkbenchLayout;
import org.jacpfx.api.message.Message;
import org.jacpfx.rcp.componentLayout.FXComponentLayout;
import org.jacpfx.rcp.context.Context;
import org.jacpfx.rcp.util.FXUtil;
import org.jacpfx.rcp.workbench.FXWorkbench;
import org.springframework.stereotype.Component;
import transit.littlecity.api.domain.StopInformation;
import transit.littlecity.api.domain.StopUpdate;
import transit.littlecity.api.handler.TransitStopHandler;
import transit.littlecity.stopview.config.StopViewConfig;

@Workbench(id = "wb1", name = "mainWorkBench",
    perspectives = StopViewConfig.STOP_VIEW_PERSPECTIVE_ID)
@Component
public class MainWorkBench implements FXWorkbench, TransitStopHandler {

    @Resource
    private Context context;

    private final Logger logger;

    public MainWorkBench(Logger logger) {
        this.logger = logger;
    }

    /**
     * Here we initialize the workbench. This is run on the javaFX application thread
     */
    @Override
    public void handleInitialLayout(Message<Event, Object> action, WorkbenchLayout<Node> layout,
                                    Stage stage) {
        logger.info("handleInitialLayout: Current thread: " + Thread.currentThread().getName());
        if (action.messageBodyEquals(FXUtil.MessageUtil.INIT)) {
            layout.setWorkbenchXYSize(0, 0);
            layout.setMenuEnabled(false);
        }
    }

    @Override
    public void postHandle(FXComponentLayout layout) {
        logger.info("postHandle: Current thread: " + Thread.currentThread().getName());
    }

    @Override
    public void initialize(StopInformation stopInformation) {
        logger.info(String.format("Stop info received: %s", stopInformation));
        context.send(StopViewConfig.STOP_VIEW_PERSPECTIVE_ID, stopInformation);
    }

    @Override
    public void handleStopUpdate(StopUpdate busStopUpdate) {
        logger.info(String.format("Got stop message: %s", busStopUpdate.getStopId()));
        context.send(StopViewConfig.STOP_VIEW_PERSPECTIVE_ID, busStopUpdate);
    }
}
