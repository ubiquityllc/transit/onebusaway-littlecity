package transit.littlecity;

import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import transit.littlecity.api.ApiService;
import transit.littlecity.launcherfx.JacpFXSpringApplicationLauncher;

@SuppressWarnings("SpringJavaAutowiredMembersInspection")
public class LittleCity extends JacpFXSpringApplicationLauncher {

    public LittleCity() {
        super(MainWorkBench.class, LittleCityAppConfig.class);
    }

    @Autowired @Qualifier("busTransitApiService")
    private ApiService busTransitService;

    @Override
    protected void beforeSpringInit(Stage stage) {
        stage.setTitle("Little City");
        stage.setHeight(1024);
        stage.setWidth(1440);
        stage.setResizable(false);
    }

    @Override
    protected void afterSpringInit(Stage stage) {
        startServices();
    }

    private void startServices() {
        busTransitService.startService();
    }
}
