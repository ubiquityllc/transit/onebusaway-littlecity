package transit.littlecity;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Import;
import transit.littlecity.logging.LoggingConfig;
import transit.littlecity.mapview.config.MapViewConfig;
import transit.littlecity.stopview.config.StopViewConfig;

/**
 * This configuration setup is mostly for adding automatic component scan for all packages
 * https://www.baeldung.com/spring-component-scanning
 */
@SpringBootConfiguration
@ComponentScan
@Import({MapViewConfig.class, StopViewConfig.class, LoggingConfig.class})
public class LittleCityAppConfig { }
